#ifndef __tcp_driver_H__
#define __tcp_driver_H__ 1
/*
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#include <qpid/dispatch/buffer.h>

#include <stdbool.h>

typedef struct tcp_driver_t tcp_driver_t;
typedef struct tcp_conn_t   tcp_conn_t;

void tcp_driver_set_context(tcp_driver_t *drv, void *context);
void *tcp_driver_get_context(const tcp_driver_t *drv);

tcp_driver_t *tcp_driver(const char *listener_address,
                         bool (*connection_requested)(tcp_driver_t *drv,
                                                      tcp_conn_t   *conn),
                         void (*connected)(tcp_driver_t *drv, tcp_conn_t *conn),
                         void (*disconnected)(tcp_driver_t *drv, tcp_conn_t *conn,
                                              qd_buffer_list_t *unsent,
                                              qd_buffer_list_t *unread),
                         void (*receive)(tcp_driver_t *drv, tcp_conn_t *conn, qd_buffer_list_t *buffs, size_t len),
                         void (*sent)(tcp_driver_t *drv, tcp_conn_t *conn, qd_buffer_list_t *buffs),
                         void (*rx_close)(tcp_driver_t *drv, tcp_conn_t *conn),
                         void (*tx_close)(tcp_driver_t *drv, tcp_conn_t *conn));
void tcp_driver_free(tcp_driver_t *drv);
void tcp_driver_run(tcp_driver_t *drv);

tcp_conn_t *tcp_driver_connect(tcp_driver_t *drv, const char *address);

void tcp_conn_set_context(tcp_conn_t *conn, void *context);
void *tcp_conn_get_context(const tcp_conn_t *conn);
void tcp_conn_disconnect(tcp_conn_t *conn);
bool tcp_conn_flow(tcp_conn_t *conn, qd_buffer_list_t *empty_buffs);
bool tcp_conn_send(tcp_conn_t *conn, qd_buffer_list_t *data, size_t offset);
#endif // __tcp_driver_H__ 1
