/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#include "tcp_driver.h"

#include <proton/raw_connection.h>
#include <proton/listener.h>
#include <proton/proactor.h>
#include <proton/netaddr.h>

#include <qpid/dispatch/ctools.h>
#include <qpid/dispatch/alloc_pool.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <unistd.h>

#define BUFF_BATCH 16

// A list of buffers containing data that starts at offset octets into
// the head buffer
typedef struct buffer_chain_t {
    DEQ_LINKS(struct buffer_chain_t);
    qd_buffer_list_t blist;
    size_t           offset;
} buffer_chain_t;
DEQ_DECLARE(buffer_chain_t, buffer_chain_list_t);
ALLOC_DECLARE(buffer_chain_t);
ALLOC_DEFINE(buffer_chain_t);


struct tcp_conn_t {
    DEQ_LINKS(tcp_conn_t);
    pn_raw_connection_t *raw_conn;
    tcp_driver_t        *driver;
    void                *user_context;
    qd_buffer_list_t     recv_buffers;
    struct {
        buffer_chain_list_t  chains;
        size_t               total;
    } send_buffers;
    unsigned int         need_disconnect:1;
    unsigned int         closed:1;
    unsigned int         rx_closed:1;
    unsigned int         tx_closed:1;
};
DEQ_DECLARE(tcp_conn_t, tcp_conn_list_t);
ALLOC_DECLARE(tcp_conn_t);
ALLOC_DEFINE(tcp_conn_t);

struct tcp_driver_t {
    char            *listener_address;
    pn_proactor_t   *proactor;
    pn_listener_t   *listener;
    void            *user_context;
    tcp_conn_list_t  connections;

    // callbacks
    bool (*connection_requested)(tcp_driver_t *drv, tcp_conn_t *conn);
    void (*connected)(tcp_driver_t *drv, tcp_conn_t *conn);
    void (*disconnected)(tcp_driver_t *drv, tcp_conn_t *conn, qd_buffer_list_t *unsent, qd_buffer_list_t *unread);
    void (*receive)(tcp_driver_t *drv, tcp_conn_t *conn, qd_buffer_list_t *buffs, size_t len);
    void (*sent)(tcp_driver_t *drv, tcp_conn_t *conn, qd_buffer_list_t *buffs);
    void (*rx_close)(tcp_driver_t *drv, tcp_conn_t *conn);
    void (*tx_close)(tcp_driver_t *drv, tcp_conn_t *conn);
};
ALLOC_DECLARE(tcp_driver_t);
ALLOC_DEFINE(tcp_driver_t);

static buffer_chain_t *buffer_chain()
{
    buffer_chain_t *chain = new_buffer_chain_t();
    ZERO(chain);
    DEQ_INIT(chain->blist);
    return chain;
}

static void buffer_chain_free(buffer_chain_t *chain)
{
    if (chain) {
        assert(DEQ_SIZE(chain->blist) == 0);
        free_buffer_chain_t(chain);
    }
}

static tcp_conn_t *tcp_conn(tcp_driver_t *drv)
{
    tcp_conn_t *conn = new_tcp_conn_t();
    ZERO(conn);
    conn->driver = drv;
    DEQ_INIT(conn->send_buffers.chains);
    DEQ_INIT(conn->recv_buffers);
    DEQ_ITEM_INIT(conn);
    DEQ_INSERT_TAIL(drv->connections, conn);
    return conn;
}


static void tcp_conn_free(tcp_conn_t *conn)
{
    if (conn) {
        assert(conn->send_buffers.total == 0);
        assert(conn->raw_conn == 0);
        DEQ_REMOVE(conn->driver->connections, conn);
        free_tcp_conn_t(conn);
    }
}


static size_t replenish_read_buffs(tcp_conn_t *conn)
{
    const size_t limit = MIN(pn_raw_connection_read_buffers_capacity(conn->raw_conn),
                             DEQ_SIZE(conn->recv_buffers));
    size_t count = 0;
    pn_raw_buffer_t buffers[BUFF_BATCH];
    while (count < limit) {
        memset(buffers, 0, sizeof(buffers));
        pn_raw_buffer_t *rdisc = &buffers[0];
        size_t batch_ct = 0;
        for (int i = 0; i < BUFF_BATCH; ++i) {
            qd_buffer_t *buf = DEQ_HEAD(conn->recv_buffers);
            assert(buf);
            DEQ_REMOVE_HEAD(conn->recv_buffers);

            buf->size = 0;
            rdisc->context  = (intptr_t)buf;
            rdisc->bytes    = (char*) qd_buffer_base(buf);
            rdisc->capacity = qd_buffer_capacity(buf);
            //rdisc->size     = 0;
            //rdisc->offset   = 0;
            ++rdisc;

            batch_ct += 1;
            count += 1;
            if (count == limit)
                break;
        }
        fprintf(stderr, "Adding %d read buffers to %p\n", (int)batch_ct, (void*) conn);
        pn_raw_connection_give_read_buffers(conn->raw_conn, buffers, batch_ct);
    }
    return count;
}


static size_t send_outgoing_buffs(tcp_conn_t *conn)
{
    pn_raw_buffer_t buffers[BUFF_BATCH];
    size_t count = pn_raw_connection_write_buffers_capacity(conn->raw_conn);
    count = MIN(count, conn->send_buffers.total);

    // Since count ensures that we never run out of capacity or buffers
    // to send we can avoid checking that on every loop

    while (count) {
        pn_raw_buffer_t *rdisc = &buffers[0];
        buffer_chain_t  *chain = DEQ_HEAD(conn->send_buffers.chains);
        assert(chain);

        memset(buffers, 0, sizeof(buffers));

        size_t batch_ct = 0;
        size_t batch_limit = MIN(BUFF_BATCH, DEQ_SIZE(chain->blist));
        batch_limit = MIN(batch_limit, count);

        while (batch_limit--) {

            qd_buffer_t *buf = DEQ_HEAD(chain->blist);
            DEQ_REMOVE_HEAD(chain->blist);

            rdisc->context  = (intptr_t)buf;
            rdisc->bytes    = (char*)qd_buffer_base(buf);
            rdisc->size     = qd_buffer_size(buf) - chain->offset;
            rdisc->offset   = chain->offset;
            ++rdisc;

            // all succeeding bufs have no offset
            chain->offset = 0;

            batch_ct += 1;
        }

        fprintf(stderr, "Adding %d write buffers to %p\n", (int)batch_ct, (void*) conn);
        pn_raw_connection_write_buffers(conn->raw_conn, buffers, batch_ct);

        assert(conn->send_buffers.total >= batch_ct);

        conn->send_buffers.total -= batch_ct;
        count -= batch_ct;

        if (DEQ_SIZE(chain->blist) == 0) {
            DEQ_REMOVE_HEAD(conn->send_buffers.chains);
        }
    }

    return count;
}


static bool handle_event(tcp_driver_t *drv, pn_event_t *event)
{
    fprintf(stdout, "EVENT %s\n", pn_event_type_name(pn_event_type(event))); fflush(stdout);

    switch (pn_event_type(event)) {
    case PN_LISTENER_OPEN: {
        char port[256];             /* Get the listening port */
        pn_netaddr_host_port(pn_listener_addr(pn_event_listener(event)), NULL, 0, port, sizeof(port));
        printf("**listening on %s\n", port);
        fflush(stdout);
        break;
    }
    case PN_LISTENER_ACCEPT: {
        pn_listener_t *listener = pn_event_listener(event);
        tcp_conn_t *conn = tcp_conn(drv);
        bool accept = false;
        if (drv->connection_requested)
            accept = drv->connection_requested(drv, conn);

        conn->raw_conn = pn_raw_connection();
        pn_listener_raw_accept(listener, conn->raw_conn);
        if (!accept) {
            // apparently the only way to refuse a connection request:
            fprintf(stderr, "**connection request refused\n");
            pn_raw_connection_close(conn->raw_conn);
            //pn_raw_connection_free(conn->raw_conn);
            conn->raw_conn = 0;
            tcp_conn_free(conn);
        } else {
            fprintf(stderr, "connection request accepted %p\n", (void*)conn);
            pn_raw_connection_set_context(conn->raw_conn, (void*) conn);
        }
    } break;


    case PN_RAW_CONNECTION_CONNECTED: {
        // new connection established
        pn_raw_connection_t *c = pn_event_raw_connection(event);
        tcp_conn_t *conn = (tcp_conn_t*) pn_raw_connection_get_context(c);
        if (conn && drv->connected)
            drv->connected(drv, conn);
    } break;

    case PN_RAW_CONNECTION_DISCONNECTED: {
        // connection disconnected - last event for connection
        pn_raw_connection_t *c = pn_event_raw_connection(event);
        tcp_conn_t *conn = (tcp_conn_t*) pn_raw_connection_get_context(c);
        fprintf(stderr, "Conn %p disconnected\n", (void*)conn);
        //pn_raw_connection_free(conn->raw_conn);
        conn->raw_conn = 0;
        if (drv->disconnected) {
            qd_buffer_list_t blist = DEQ_EMPTY;
            buffer_chain_t *chain = DEQ_HEAD(conn->send_buffers.chains);
            while (chain) {
                DEQ_REMOVE_HEAD(conn->send_buffers.chains);
                DEQ_APPEND(blist, chain->blist);
                buffer_chain_free(chain);
                chain = DEQ_HEAD(conn->send_buffers.chains);
            }
            drv->disconnected(drv, conn, &blist, &conn->recv_buffers);
        }
        tcp_conn_free(conn);
    } break;

    case PN_RAW_CONNECTION_NEED_READ_BUFFERS: {
        pn_raw_connection_t *c = pn_event_raw_connection(event);
        tcp_conn_t *conn = (tcp_conn_t *) pn_raw_connection_get_context(c);
        assert(conn);
        fprintf(stderr, "Conn %p need read buf\n", (void*) conn);
        if (conn->raw_conn && !pn_raw_connection_is_read_closed(conn->raw_conn))
            replenish_read_buffs(conn);
    } break;

    case PN_RAW_CONNECTION_NEED_WRITE_BUFFERS: {
        pn_raw_connection_t *c = pn_event_raw_connection(event);
        tcp_conn_t *conn = (tcp_conn_t *) pn_raw_connection_get_context(c);
        assert(conn);
        fprintf(stderr, "Conn %p need write buf\n", (void*) conn);
        if (conn->raw_conn && !pn_raw_connection_is_write_closed(conn->raw_conn))
            send_outgoing_buffs(conn);
    } break;

    case PN_RAW_CONNECTION_READ: {
        // give read data to application
        pn_raw_connection_t *c = pn_event_raw_connection(event);
        tcp_conn_t *conn = (tcp_conn_t *) pn_raw_connection_get_context(c);
        assert(conn);
        pn_raw_buffer_t buffs[BUFF_BATCH];

        qd_buffer_list_t blist = DEQ_EMPTY;
        size_t len = 0;
        size_t n;
        while ((n = pn_raw_connection_take_read_buffers(c, buffs, BUFF_BATCH)) != 0) {
            for (size_t i = 0; i < n; ++i) {
                qd_buffer_t *qd_buf = (qd_buffer_t*)buffs[i].context;
                assert(qd_buf);
                // set content length:
                qd_buffer_insert(qd_buf, buffs[i].size);
                len += buffs[i].size;
                DEQ_INSERT_TAIL(blist, qd_buf);
            }
        }

        if (DEQ_SIZE(blist) > 0) {
            fprintf(stderr, "Conn %p data read %d buffers %zu bytes\n",
                    (void*) conn, (int)DEQ_SIZE(blist), len);
            drv->receive(drv, conn, &blist, len);
        }
    } break;

    case PN_RAW_CONNECTION_WRITTEN: {
        // return empty write buffers
        pn_raw_connection_t *c = pn_event_raw_connection(event);
        tcp_conn_t *conn = (tcp_conn_t *) pn_raw_connection_get_context(c);
        assert(conn);
        fprintf(stderr, "Conn %p data written\n", (void*) conn);
        pn_raw_buffer_t buffs[BUFF_BATCH];
        qd_buffer_list_t blist = DEQ_EMPTY;
        size_t n;
        while ((n = pn_raw_connection_take_written_buffers(conn->raw_conn, buffs, BUFF_BATCH)) != 0) {
            for (size_t i = 0; i < n; ++i) {
                qd_buffer_t *qd_buf = (qd_buffer_t*)buffs[i].context;
                assert(qd_buf);
                qd_buf->size = 0;
                DEQ_INSERT_TAIL(blist, qd_buf);
            }
        }

        if (DEQ_SIZE(blist) > 0) {
            fprintf(stderr, "Conn %p data written %d buffers\n", (void*) conn, (int)DEQ_SIZE(blist));
            drv->sent(drv, conn, &blist);
        }
    } break;

    case PN_RAW_CONNECTION_CLOSED_WRITE: {
        // remote closed our outgoing (WRITE) stream
        pn_raw_connection_t *c = pn_event_raw_connection(event);
        tcp_conn_t *conn = (tcp_conn_t *) pn_raw_connection_get_context(c);
        assert(conn);
        fprintf(stderr, "Conn %p write closed\n", (void*) conn);
        if (drv->tx_close && !conn->tx_closed) {
            conn->tx_closed = 1;
            drv->tx_close(drv, conn);
            if (!conn->closed) {
                conn->closed = 1;
                pn_raw_connection_close(c);
            }
        }
    } break;

    case PN_RAW_CONNECTION_CLOSED_READ: {
        // remote closed our incoming (READ) stream
        pn_raw_connection_t *c = pn_event_raw_connection(event);
        tcp_conn_t *conn = (tcp_conn_t *) pn_raw_connection_get_context(c);
        assert(conn);
        fprintf(stderr, "Conn %p read closed\n", (void*) conn);
        if (drv->rx_close && !conn->rx_closed) {
            conn->rx_closed = 1;
            drv->rx_close(drv, conn);
            if (!conn->closed) {
                conn->closed = 1;
                pn_raw_connection_close(c);
            }
        }
    } break;

    case PN_RAW_CONNECTION_WAKE: {
        pn_raw_connection_t *c = pn_event_raw_connection(event);
        tcp_conn_t *conn = (tcp_conn_t*) pn_raw_connection_get_context(c);
        assert(conn);
        fprintf(stderr, "Conn %p wake\n", (void*) conn);
        if (conn->raw_conn) {
            if (conn->need_disconnect) {
                fprintf(stderr, "Conn %p need disconnect\n", (void*) conn);
                if (!conn->closed) {
                    conn->closed = 1;
                    pn_raw_connection_close(conn->raw_conn);
                    fprintf(stderr, "Conn %p closing\n", (void*) conn);
                }
            } else {
                if (!pn_raw_connection_is_read_closed(conn->raw_conn))
                    replenish_read_buffs(conn);
                if (!pn_raw_connection_is_write_closed(conn->raw_conn))
                    send_outgoing_buffs(conn);
            }
        }
    } break;

    default:
        fprintf(stderr, "EVENT INGORED!!\n");
        break;
    }

    return true;
}


tcp_driver_t *tcp_driver(const char *listener_address,
                         bool (*connection_requested)(tcp_driver_t *drv,
                                                      tcp_conn_t   *conn),
                         void (*connected)(tcp_driver_t *drv, tcp_conn_t *conn),
                         void (*disconnected)(tcp_driver_t *drv, tcp_conn_t *conn, qd_buffer_list_t *unsent, qd_buffer_list_t *unread),
                         void (*receive)(tcp_driver_t *drv, tcp_conn_t *conn, qd_buffer_list_t *buffs, size_t len),
                         void (*sent)(tcp_driver_t *drv, tcp_conn_t *conn, qd_buffer_list_t *buffs),
                         void (*rx_close)(tcp_driver_t *drv, tcp_conn_t *conn),
                         void (*tx_close)(tcp_driver_t *drv, tcp_conn_t *conn))
{
    tcp_driver_t *drv = new_tcp_driver_t();
    ZERO(drv);
    drv->listener_address = strdup(listener_address);
    drv->proactor = pn_proactor();
    drv->listener = pn_listener();

    drv->connection_requested = connection_requested;
    drv->connected = connected;
    drv->disconnected = disconnected;
    drv->receive = receive;
    drv->sent = sent;
    drv->rx_close = rx_close;
    drv->tx_close = tx_close;

    pn_proactor_listen(drv->proactor, drv->listener, drv->listener_address, 16);
    return drv;
}


void tcp_driver_free(tcp_driver_t *drv)
{
    if (drv) {
        pn_proactor_free(drv->proactor);
        free(drv->listener_address);
        free_tcp_driver_t(drv);
    }
}


void tcp_driver_run(tcp_driver_t *drv)
{
    /* Loop and handle events */
    do {
        pn_event_batch_t *events = pn_proactor_wait(drv->proactor);
        for (pn_event_t *e = pn_event_batch_next(events);
             e;
             e = pn_event_batch_next(events)) {

            if (!handle_event(drv, e)) {
                return;
            }
        }
        pn_proactor_done(drv->proactor, events);
    } while (true);
}


void tcp_driver_set_context(tcp_driver_t *drv, void *context)
{
    assert(drv);
    drv->user_context = context;
}


void *tcp_driver_get_context(const tcp_driver_t *drv)
{
    assert(drv);
    return drv->user_context;
}


tcp_conn_t *tcp_driver_connect(tcp_driver_t *drv, const char *address)
{
    assert(drv && address);
    tcp_conn_t *conn = tcp_conn(drv);

    conn->raw_conn = pn_raw_connection();
    pn_raw_connection_set_context(conn->raw_conn, (void*) conn);
    pn_proactor_raw_connect(drv->proactor, conn->raw_conn, address);

    return (tcp_conn_t*) conn;
}


void tcp_conn_set_context(tcp_conn_t *conn, void *context)
{
    assert(conn);
    conn->user_context = context;
}


void *tcp_conn_get_context(const tcp_conn_t *conn)
{
    assert(conn);
    return conn->user_context;
}


void tcp_conn_disconnect(tcp_conn_t *conn)
{
    assert(conn);
    if (conn->raw_conn && !conn->need_disconnect) {
        fprintf(stderr, "Schedule disconnect %p\n", (void *)conn);
        conn->need_disconnect = 1;
        pn_raw_connection_wake(conn->raw_conn);
    }
}


bool tcp_conn_flow(tcp_conn_t *conn, qd_buffer_list_t *empty_buffs)
{
    assert(conn);
    assert(empty_buffs);
    if (conn->raw_conn && !pn_raw_connection_is_read_closed(conn->raw_conn)) {
        fprintf(stderr, "Schedule new read buffers %p\n", (void *)conn);
        DEQ_APPEND(conn->recv_buffers, *empty_buffs);
        pn_raw_connection_wake(conn->raw_conn);
        return true;
    }
    return false;
}


bool tcp_conn_send(tcp_conn_t *conn, qd_buffer_list_t *data, size_t offset)
{
    assert(conn);
    assert(data);
    if (conn->raw_conn && !pn_raw_connection_is_write_closed(conn->raw_conn)) {
        fprintf(stderr, "Schedule new write buffers %p\n", (void *)conn);
        buffer_chain_t *chain = buffer_chain();
        chain->offset = offset;
        chain->blist = *data;
        DEQ_INIT(*data);
        conn->send_buffers.total += DEQ_SIZE(chain->blist);
        DEQ_INSERT_TAIL(conn->send_buffers.chains, chain);
        pn_raw_connection_wake(conn->raw_conn);
        return true;
    }

    return false;
}
