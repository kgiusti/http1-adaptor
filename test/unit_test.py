#!/usr/bin/env python
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License
#

try:
    from http.server import HTTPServer, BaseHTTPRequestHandler
except ImportError:
    from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler

import sys
from threading import Thread


class HttpResponse(object):
    def __init__(self, status, version=None, reason=None,
                 headers=None, body=None, eom_close=False, error=False):
        self.status = status
        self.version = version or "HTTP/1.1"
        self.reason = reason
        self.headers = headers or []
        self.body = body
        self.eom_close = eom_close
        self.error = error

    def do_response(self, handler):
        handler.protocol_version = self.version
        if self.error:
            handler.send_error(self.status,
                               message=self.reason,
                               explain=self.body)
            return

        handler.send_response(self.status, self.reason)
        for header in self.headers:
            handler.send_header(header[0], header[1])
        handler.end_headers()

        if self.body:
            handler.wfile.write(self.body)

        if self.eom_close:
            print("Closing connection at EOM")
            handler.server.shutdown()
            handler.server.server_close()


GET_RESPONSES = {
    "/get_error":
    HttpResponse(400, reason="Bad breath", error=True),

    "/get_content_len":
    HttpResponse(200, reason="OK",
                 headers=[("Content-Length", 1),
                          ("Content-Type", "text/plain;charset=utf-8")],
                 body=b'?'),

    "/get_content_len_513":
    HttpResponse(200, reason="OK",
                 headers=[("Content-Length", 512),
                          ("Content-Type", "text/plain;charset=utf-8")],
                 body=b'X' * 512),

    "/get_chunked":
    HttpResponse(200, reason="OK",
                 headers=[("transfer-encoding", "chunked"),
                          ("Content-Type", "text/plain;charset=utf-8")],
                 # note: the chunk length does not count the trailing CRLF
                 body=b'15\r\n'
                 + b'Mary had a little pug\r\n'
                 + b'1b\r\n'
                 + b'Its name was "Skupper-Jack"\r\n'
                 + b'0\r\n'
                 + b'Optional: Trailer\r\n'
                 + b'Optional: Trailer\r\n'
                 + b'\r\n')
    
}




class RequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        print("GET %s (%s:%s)" % (self.path, self.client_address[0],
                                  self.client_address[1]))

        rsp = GET_RESPONSES.get(self.path)
        if rsp is None:
            rsp = HttpResponse(404, reason="Not Found", error=True)

        rsp.do_response(self)


class TestServer(object):
    def __init__(self, port=8080):
        self._server_addr = ("", port)
        self._server = HTTPServer(self._server_addr, RequestHandler)
        self._stop_thread = False
        self._thread = Thread(target=self._run)
        self._thread.daemon = True
        self._thread.start()

    def _run(self):
        print("TestServer listening on 0.0.0.0:%d" % self._server_addr[1])
        self._server.serve_forever()

    def wait(self):
        self._thread.join()

def main(argv):
    httpd = TestServer()
    try:
        httpd.wait();
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    sys.exit(main(argv=sys.argv))
