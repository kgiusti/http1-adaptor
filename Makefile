C_FLAGS = -O0 -g -Wall -I. -lqpid-dispatch -lqpid-proton -lm

##BUILD_OPTS = -I./include -I/home/kgiusti/work/dispatch/adaptors/INSTALL/include -L/home/kgiusti/work/dispatch/adaptors/INSTALL/lib64 -L/home/kgiusti/work/dispatch/adaptors/INSTALL/lib/qpid-dispatch

BUILD_OPTS = -I./include -I ${QPID_DISPATCH_INCLUDES} -I ${QPID_PROTON_INCLUDES} -L ${QPID_DISPATCH_LIB} -L ${QPID_PROTON_LIB}


all: http1_proxy

clean:
	rm -f http1_lib.o
	rm -f tcp_driver.o
	rm -f http1_proxy.o
	rm -f http1_proxy

http1_lib.o: include/http1_lib.h src/http1_lib.c
	gcc $(BUILD_OPTS) $(C_FLAGS) -c src/http1_lib.c

tcp_driver.o: src/tcp_driver.c include/tcp_driver.h
	gcc $(BUILD_OPTS) $(C_FLAGS) -c src/tcp_driver.c

http1_proxy.o: src/http1_proxy.c include/tcp_driver.h include/http1_lib.h
	gcc $(BUILD_OPTS) $(C_FLAGS) -c src/http1_proxy.c

http1_proxy: http1_proxy.o tcp_driver.o http1_lib.o
	gcc $(BUILD_OPTS) $(C_FLAGS) http1_proxy.o http1_lib.o tcp_driver.o -o http1_proxy
