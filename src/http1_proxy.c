/*
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#include <http1_lib.h>
#include <tcp_driver.h>

//#include <qpid/dispatch/ctools.h>
#include <qpid/dispatch/alloc_pool.h>
#include <unistd.h>

#define MAX_STREAMS 5
#define MAX_RX_BUFFS 100


static int exit_code = 0;
char *server_address = "127.0.0.1:80";
char *listener_address = "0.0.0.0:8080";

typedef struct conn_data_t {
    http1_conn_t        *http_conn;
    tcp_conn_t          *tcp_conn;
    char                 name[32];
    struct conn_data_t  *peer;
    enum {CLIENT, SERVER} role;
    struct http1_transfer_t *xfer;
    unsigned int         outstanding_buffers;
    bool                 need_close;
} conn_data_t;

typedef struct stream_t {
    conn_data_t  client;
    conn_data_t  server;
} stream_t;

typedef struct http_proxy_t
{
    tcp_driver_t *tcp_driver;
    stream_t      streams[MAX_STREAMS];
} http_proxy_t;


static void usage(const char *proggie)
{
    printf("Usage: %s <options>\n", proggie);
    printf("-s  \tThe address:port of the remote server [%s]\n", server_address);
    printf("-l  \tThe address:port for client connections [%s]\n", listener_address);
    exit(1);
}


static void parse_args(int argc, char *argv[])
{
    opterr = 0;
    int c;
    while ((c = getopt(argc, argv, "hs:l:")) != -1) {
        switch(c) {
        case 'h': usage(argv[0]); break;
        case 's': server_address = optarg; break;
        case 'l': listener_address = optarg; break;
        default:
            usage(argv[0]);
            break;
        }
    }
}

//
// HTTP CALLBACKS
//

static void http_conn_send_data_cb(http1_conn_t *http_conn, qd_buffer_list_t *data, size_t offset, unsigned int len)
{
    conn_data_t *conn = (conn_data_t *)http1_connection_get_context(http_conn);
    assert(conn);
    size_t count = DEQ_SIZE(*data);

    fprintf(stderr, "Send data http callback %s bufs=%zu offset=%zu len=%u\n",
            conn->name, DEQ_SIZE(*data), offset, len);

    if (!conn->tcp_conn || !tcp_conn_send(conn->tcp_conn, data, offset)) {
        fprintf(stderr, "Send failed!\n");
        qd_buffer_list_free_buffers(data);
    } else {
        conn->outstanding_buffers += count;
    }
    assert(DEQ_IS_EMPTY(*data));
}

void http_conn_error_cb(http1_conn_t *http_conn, int code, const char *reason)
{
    conn_data_t *conn = (conn_data_t *)http1_connection_get_context(http_conn);
    assert(conn);
    fprintf(stderr, "Conn %s HTTP ERROR: code=%d reason='%s'\n", conn->name, code, reason);
}


static int http_msg_request_cb(http1_transfer_t *xfer, const char *method, const char *target, const char *version)
{
    conn_data_t *conn = (conn_data_t*) http1_connection_get_context(http1_transfer_get_connection(xfer));
    assert(conn);
    fprintf(stderr, "Conn %s HTTP REQUEST RECEIVED method='%s' target='%s' version='%s' xfer=%p\n",
            conn->name, method, target, version, (void*)xfer);
    assert(!conn->xfer);
    http1_transfer_set_context(xfer, (void*)conn);
    conn->xfer = xfer;

    // forward request to peer

    conn_data_t *peer = conn->peer;
    assert(!peer->xfer);

    fprintf(stderr, "  FWD HTTP REQUEST TO %s\n", peer->name);

    peer->xfer = http1_tx_request(peer->http_conn, method, target, version);
    assert(peer->xfer);
    http1_transfer_set_context(peer->xfer, (void*)peer);
    fprintf(stderr, "  %s new transfer %p\n", peer->name, peer->xfer);

    return 0;
}


static int http_msg_response_cb(http1_transfer_t *xfer, const char *version, int status_code, const char *reason_phrase)
{
    conn_data_t *conn = (conn_data_t*) http1_transfer_get_context(xfer);
    assert(conn);
    fprintf(stderr, "Conn %s HTTP RESPONSE RECEIVED version='%s' status=%d reason='%s' xfer=%p\n",
            conn->name, version, status_code, reason_phrase, (void*)xfer);
    assert(conn->xfer == xfer);

    conn_data_t *peer = conn->peer;
    assert(peer->xfer);

    fprintf(stderr, "  FWD HTTP RESPONSE BACK TO %s %p\n", peer->name, peer->xfer);

    int rc = http1_tx_response(peer->xfer, version, status_code, reason_phrase);
    assert(rc == 0);

    return 0;
}


static int http_msg_header_cb(http1_transfer_t *xfer, const char *key, const char *value)
{
    conn_data_t *conn = (conn_data_t*) http1_transfer_get_context(xfer);
    assert(conn);
    assert(conn->xfer == xfer);
    fprintf(stderr, "Conn %s HTTP MSG HEADER key='%s' value='%s' xfer=%p\n",
            conn->name, key, value, (void*)xfer);

    conn_data_t *peer = conn->peer;
    assert(peer->xfer);

    fprintf(stderr, "  FWD HTTP MSG HDR BACK TO %s %p\n", peer->name, (void*)xfer);

    int rc = http1_tx_add_header(peer->xfer, key, value);
    assert(rc == 0);

    return 0;

}

static int http_msg_headers_done_cb(http1_transfer_t *xfer)
{
    conn_data_t *conn = (conn_data_t*) http1_transfer_get_context(xfer);
    assert(conn);
    assert(conn->xfer == xfer);
    fprintf(stderr, "Conn %s HTTP MSG HEADERS DONE %p\n", conn->name, conn->xfer);
    return 0;
}


static int http_msg_body_cb(http1_transfer_t *xfer, qd_buffer_list_t *body, size_t offset, size_t len)
{
    conn_data_t *conn = (conn_data_t*) http1_transfer_get_context(xfer);
    assert(conn);
    assert(conn->xfer == xfer);

    fprintf(stderr, "Conn %s HTTP BODY bufs=%zu offset=%zu %zu bytes %p\n", conn->name, DEQ_SIZE(*body), offset, len,
            (void*)conn->xfer);

    conn_data_t *peer = conn->peer;
    assert(peer->xfer);

    fprintf(stderr, "  FWD HTTP BODY TO %s %p\n", peer->name, (void*)peer->xfer);

    int rc = http1_tx_body(peer->xfer, body, offset, len);
    assert(rc == 0);

    return 0;
}



static void http_msg_done_cb(http1_transfer_t *xfer)
{
    conn_data_t *conn = (conn_data_t*) http1_transfer_get_context(xfer);
    assert(conn);
    assert(conn->xfer == xfer);
    fprintf(stderr, "Conn %s HTTP MSG DONE %p\n", conn->name, (void*)conn->xfer);

    conn_data_t *peer = conn->peer;
    assert(peer->xfer);

    fprintf(stderr, "  FWD HTTP MSG DONE TO %s %p\n", peer->name, peer->xfer);

    int rc = http1_tx_done(peer->xfer);
    assert(rc == 0);
}


static void http_transfer_done_cb(http1_transfer_t *xfer)
{
    conn_data_t *conn = (conn_data_t*) http1_transfer_get_context(xfer);
    assert(conn);
    assert(conn->xfer == xfer);
    fprintf(stderr, "Conn %s HTTP TRANSFER DONE %p\n", conn->name, conn->xfer);
    conn->xfer = 0;
}

//
// TCP CALLBACKS
//

// tcp conn from client
static bool connection_requested(tcp_driver_t *drv, tcp_conn_t *tcp_conn)
{
    http_proxy_t *app = tcp_driver_get_context(drv);
    assert(app);

    // new incoming client - find the next free stream
    conn_data_t *client = 0;
    for (int i = 0; i < MAX_STREAMS; ++i) {
        if (!app->streams[i].client.tcp_conn &&
            !app->streams[i].server.tcp_conn) {
            client = &app->streams[i].client;
            break;
        }
    }

    if (!client) {  // maxed out
        fprintf(stderr, "**connection request denied - max capacity\n");
        fflush(stderr);
        return false;
    }

    fprintf(stderr, "Connection request for %s received\n", client->name);

    assert(!client->http_conn);

    client->tcp_conn = tcp_conn;
    client->outstanding_buffers = 0;
    client->need_close = false;
    tcp_conn_set_context(tcp_conn, (void*) client);
    fprintf(stderr, "Accepting client connection %s\n", client->name);

    // initiate peer connection to server

    conn_data_t *server = client->peer;
    assert(!server->tcp_conn);

    server->tcp_conn = tcp_driver_connect(drv, server_address);
    server->outstanding_buffers = 0;
    server->need_close = false;
    tcp_conn_set_context(server->tcp_conn, (void*)server);
    fprintf(stderr, "Connecting to %s\n", server->name);

    return true;
}


// Connection to server completed
//
static void connected(tcp_driver_t *drv, tcp_conn_t *tcp_conn)
{
    conn_data_t *conn = (conn_data_t*) tcp_conn_get_context(tcp_conn);
    assert(conn);
    fprintf(stderr, "%s connected\n", conn->name);

    bool is_server = (conn->role == SERVER);

    http1_conn_config_t config = {0};

    config.type            = is_server ? HTTP1_CONN_SERVER : HTTP1_CONN_CLIENT;

    config.conn_tx_data          = http_conn_send_data_cb;
    config.xfer_rx_request       = http_msg_request_cb;
    config.xfer_rx_response      = http_msg_response_cb;
    config.xfer_rx_header        = http_msg_header_cb;
    config.xfer_rx_headers_done  = http_msg_headers_done_cb;
    config.xfer_rx_body          = http_msg_body_cb;
    config.xfer_rx_done          = http_msg_done_cb;
    config.xfer_done             = http_transfer_done_cb;

    conn->http_conn = http1_connection(&config, (void*) conn);
    if (!conn->http_conn) {
        fprintf(stderr, "**connection request denied - http connection refused\n");
        fflush(stderr);
    }

    //
    // Once both sides of the stream have established the connection grant buffers for the receive pools
    //

    conn_data_t *peer = conn->peer;

    if (peer->http_conn) {

        fprintf(stderr, "Granting RX buffers to %s and %s\n", conn->name, peer->name);
        qd_buffer_list_t blist = DEQ_EMPTY;
        for (int i = 0; i < MAX_RX_BUFFS; ++i) {
            qd_buffer_t *buf = qd_buffer();
            DEQ_INSERT_TAIL(blist, buf);
        }
        bool ok = tcp_conn_flow(conn->tcp_conn, &blist);
        assert(ok);

        DEQ_INIT(blist);
        for (int i = 0; i < MAX_RX_BUFFS; ++i) {
            qd_buffer_t *buf = qd_buffer();
            DEQ_INSERT_TAIL(blist, buf);
        }
        ok = tcp_conn_flow(peer->tcp_conn, &blist);
        assert(ok);
    }
}


// client or server disconnected
//
static void disconnected(tcp_driver_t *drv, tcp_conn_t *tcp_conn,
                         qd_buffer_list_t *unsent,
                         qd_buffer_list_t *unread)
{
    conn_data_t *conn = (conn_data_t*) tcp_conn_get_context(tcp_conn);
    assert(conn);
    fprintf(stderr, "Conn %s disconnected\n", conn->name);
    fprintf(stderr, "  %d unsent %d unread\n", (int)DEQ_SIZE(*unsent), (int)DEQ_SIZE(*unread));

    qd_buffer_list_free_buffers(unsent);
    qd_buffer_list_free_buffers(unread);
    conn->tcp_conn = 0;

    if (conn->http_conn) {
        http1_connection_close(conn->http_conn);
        conn->http_conn = 0;
    }

    conn_data_t *peer = conn->peer;
    if (peer->tcp_conn) {
        if (peer->outstanding_buffers == 0) {
            fprintf(stderr, "Disconnecting peer %s\n", peer->name);
            tcp_conn_disconnect(peer->tcp_conn);
        } else {
            fprintf(stderr, "Pending peer %s disconnect %u buffers outstanding\n", peer->name,
                peer->outstanding_buffers);
            peer->need_close = true;
        }
    }
}


static void receive(tcp_driver_t *drv, tcp_conn_t *tcp_conn, qd_buffer_list_t *buffs, size_t len)
{
    conn_data_t *conn = (conn_data_t*) tcp_conn_get_context(tcp_conn);
    assert(conn);
    size_t buf_ct = DEQ_SIZE(*buffs);
    fprintf(stderr, "Conn %s data received %d buffers\n", conn->name, (int)buf_ct);

    if (buf_ct == 0) return;

    if (conn->http_conn) {
        int rc = http1_connection_rx_data(conn->http_conn, buffs, len);
        if (rc) {
            fprintf(stderr, "  cannot forward bufs: http error %d\n", rc);
        }

        //
        // replenish bufs
        //
        qd_buffer_list_t blist = DEQ_EMPTY;
        while (buf_ct--) {
            qd_buffer_t *buf = qd_buffer();
            DEQ_INSERT_TAIL(blist, buf);
        }
        tcp_conn_flow(conn->tcp_conn, &blist);

    } else if (tcp_conn_flow(conn->tcp_conn, buffs)) {
        // cannot forward them - return to receive pool
        fprintf(stderr, "  cannot forward bufs: http closed\n");

    } else {
        fprintf(stderr, "  cannot return bufs - discarding bufs received by %s\n", conn->name);
        qd_buffer_list_free_buffers(buffs);
    }
}


static void sent(tcp_driver_t *drv, tcp_conn_t *tcp_conn, qd_buffer_list_t *buffs)
{
    conn_data_t *conn = (conn_data_t*) tcp_conn_get_context(tcp_conn);
    size_t count = DEQ_SIZE(*buffs);
    assert(conn);
    fprintf(stderr, "Conn %s WRITTEN %d buffs (freed)\n", conn->name, (int)DEQ_SIZE(*buffs));
    assert(count <= conn->outstanding_buffers);
    conn->outstanding_buffers -= count;
    if (conn->outstanding_buffers == 0 && conn->need_close) {
        fprintf(stderr, "Pending peer %s buffers flushed - disconnecting\n", conn->name);
        tcp_conn_disconnect(tcp_conn);
    }
    qd_buffer_list_free_buffers(buffs);
}


static void rx_close(tcp_driver_t *drv, tcp_conn_t *tcp_conn)
{
    conn_data_t *conn = (conn_data_t*) tcp_conn_get_context(tcp_conn);
    assert(conn);
    fprintf(stderr, "Conn %s read closed\n", conn->name);
    //tcp_conn_disconnect(tcp_conn);
    //if (conn->http_conn) {
    //http1_connection_close(conn->http_conn);
    //conn->http_conn = 0;
//}
}


static void tx_close(tcp_driver_t *drv, tcp_conn_t *tcp_conn)
{
    conn_data_t *conn = (conn_data_t*) tcp_conn_get_context(tcp_conn);
    assert(conn);
    fprintf(stderr, "Conn %s write closed\n", conn->name);
    //tcp_conn_disconnect(tcp_conn);
    //if (conn->http_conn) {
    //http1_connection_close(conn->http_conn);
    //conn->http_conn = 0;
// }
}


int main(int argc, char *argv[])
{
    http_proxy_t app = {0};

    qd_alloc_initialize();

    parse_args(argc, argv);

    // initialize streams
    for (int i = 0; i < MAX_STREAMS; ++i) {
        stream_t *stream = &app.streams[i];
        snprintf(stream->client.name, sizeof(stream->client.name), "client-%d", i);
        stream->client.peer = &stream->server;
        stream->client.role = CLIENT;

        snprintf(stream->server.name, sizeof(stream->server.name), "server-%d", i);
        stream->server.peer = &stream->client;
        stream->server.role = SERVER;
    }

  // create the tcp driver

  app.tcp_driver = tcp_driver(listener_address,
                              connection_requested,
                              connected,
                              disconnected,
                              receive,
                              sent,
                              rx_close,
                              tx_close);
  tcp_driver_set_context(app.tcp_driver, (void *)&app);
  tcp_driver_run(app.tcp_driver);
  tcp_driver_free(app.tcp_driver);

  qd_alloc_finalize();
  return exit_code;
}

